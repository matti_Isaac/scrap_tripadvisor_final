"""
Authors : Matthias ELBAZ, Dan BONAN
Date: 26/05/2019

The program tripadvisor.py scrapp information about
restaurants in the city of Tel-Aviv:
name
number of reviews
ratings
index in the site
price scale
phone number
address
type of cuisine
url
longitue
latitude

It will register all this information in a DataBase

The program asks to the user a number of page and it
will scrapp from the first page to the page indicated all
restaurants features. If a features do not find it will
store a NaN value.

Becarefull!! the scrapping can take a long time!!
"""


import requests
from bs4 import BeautifulSoup
import re
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait as wait
from selenium.webdriver.support import expected_conditions as EC
from selenium import webdriver
import sys
import pymysql
from googleplaces import GooglePlaces, types, lang


# CONSTANTS
BEGIN_OF_URL = "https://www.tripadvisor.com"
URL_PAGE_1 = "https://www.tripadvisor.com/Restaurants-g293984-Tel_Aviv_Tel_Aviv_District.html"
URL_PAGE_N = "https://www.tripadvisor.com/RestaurantSearch-g293984-oa|N|-Tel_Aviv_Tel_Aviv_District."
NUMBER_RESTAURANTS = 30
DB_TRIPADVISOR = 'db_tripadvisor'
API_KEY = ''# have to declare your API_KEY from GOOGLE


# OPEN DRIVER
driver = webdriver.Chrome('/home/elbaz/Bureau/ITC_DOC/Courses/Project/Data Mining/chromedriver')


# GET FEATURES
# get url of a page
def get_url_of_page_n(num_page):
    """function get_url takes an integer and return the url at page representing by
    the integer"""
    if num_page == 1:
        return URL_PAGE_1
    else:
        url_split = URL_PAGE_N.split('|')
        url_split[1] = str((num_page-1)*30)
    url_page_n = ''.join(url_split)
    return url_page_n


# get features from selenium
def restaurant_details(num_resto):
    """function which return a text of all information about a restaurant pointed by its number"""
    copiedText = wait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, "div.listing.rebrand.listingIndex-"+str(num_resto)))).text
    return copiedText


def rating(num_resto):
    """function which returns the rating of a restautrant defines by its number"""
    rating_element = driver.find_elements_by_xpath('//div[2]/div[1]/div[2]/span[1]')
    return rating_element[num_resto].get_attribute('alt')


def url_restaurant(num_page, num_resto):
    """function which returns the url of the restaurant defines by its number"""
    url_element = driver.find_elements_by_xpath('//div[2]/div[1]/div[1]/a')
    if num_page == 1:
        return url_element[num_resto + 6].get_attribute('href')
    else:
        return url_element[num_resto + 5].get_attribute('href')


# get features from b4s
def make_soup(url):
    """function make_soup takes an url and return the soup object"""
    source = requests.get(url)
    soup = BeautifulSoup(source.content, 'html.parser')
    return soup


def get_address(soup_resto):
    """function get_address takes the soup of a restaurant
    and return its address"""
    containers = soup_resto.find('span', class_='street-address')
    if not containers:
        address = 'Unknown'
    else:
        address = list(containers)[0]
    return address


def get_phone(soup_resto):
    """function get_phone takes the soup of a restaurant
        and return its phone"""
    containers = soup_resto.find_all(string=re.compile(r'\+972'))
    if not containers:
        phone = 'Unknown'
    else:
        phone = list(containers)[0]
    return phone


# STORE FEATURES
# data from api
def lat_lng(name):
    API_KEY = API_KEY
    google_places = GooglePlaces(API_KEY)
    query_result = google_places.nearby_search(name=name, location='Tel Aviv', keyword='Restaurants', radius=10000, types=[types.TYPE_RESTAURANT])
    lat = 0
    lng = 0
    for place in query_result.places:
        lat = float(place.geo_location['lat'])
        lng = float(place.geo_location['lng'])
        break
    return lat, lng


# store features of a restaurant in a list
def features_list(num_page, num_resto):
    """Function which store in a list all features for one
    restaurant defines by its number"""
    restaurant_text = restaurant_details(num_resto).split('\n')
    if restaurant_text[0].startswith('Travelers') is True:
        restaurant_text = restaurant_text[1:]
    if restaurant_text[3].startswith('#') is True:
        del restaurant_text[3]
    #
    rate = rating(num_resto)
    names = restaurant_text[0]
    reviews = restaurant_text[1].split(' ')[0]
    index = restaurant_text[2].split(' ')[0]
    if re.match("(\$+(.+\$+)?)", restaurant_text[3]) == None:
        price = 'Unknown'
    else:
        price = re.match("(\$+(.+\$+)?)", restaurant_text[3]).group(0)
    cuisine = restaurant_text[3].strip('$ -')
    url_resto = url_restaurant(num_page, num_resto)
    #
    soup_resto = make_soup(url_resto)
    address = get_address(soup_resto)
    phone = get_phone(soup_resto)
    #
    latitude, longitude = lat_lng(names)
    return [names, int(reviews.replace(',', '')), float(rate.split()[0]), int(index[1:]), price, cuisine, url_resto, address, phone, latitude, longitude]


# STORE IN DATABASE
def insert_in_database(database, line):
    username = 'root'
    connection = pymysql.connect(user=username, password='Macrogol16/', database=database)
    cur = connection.cursor()
    cur.execute("INSERT INTO Restaurant\
                (id, name, address, telephone, url, price, latitude, longitude) VALUES (%s,%s,%s,%s,%s,%s,%s,%s)",
                (line[3], line[0], line[7], line[8], line[6], line[4], line[9], line[10]))
    connection.commit()
    cur.execute("INSERT INTO Cuisine\
                (type, id_resto) VALUES (%s,%s)",
                (line[5], line[3]))
    connection.commit()
    cur.execute("INSERT INTO Rate\
                (rating, reviews, id_resto) VALUES (%s,%s,%s)",
                (line[2], line[1], line[3]))
    connection.commit()
    cur.close()


# store in database
def store_features_for_all_restaurants(num_page, num_resto):
    """Function which the list of features of a restaurants in a list"""
    for i in range(num_page):
        print('page: ', i+1)
        url_page = get_url_of_page_n(i+1)
        driver.get(url_page)
        for j in range(num_resto):
            print('resto: ', j+1)
            line = features_list(i+1, j+1)
            insert_in_database(DB_TRIPADVISOR, line)


def main():
    """In the terminal, the parameters are the python program and
    the number of the page we want to stop the scrapping."""
    choose_number_of_pages = sys.argv[1]
    if not choose_number_of_pages:
        print('usage: nb_page')
        sys.exit(1)

    store_features_for_all_restaurants(int(choose_number_of_pages), NUMBER_RESTAURANTS)


if __name__ == '__main__':
    main()
