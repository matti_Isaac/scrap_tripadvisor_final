"""
Authors : Matthias ELBAZ, Dan BONAN
Date: 10/06/2019

The program data_base.py fills the database data_tripadvisor.db.
It contains 3 tables:
- table Restaurant : id (primary key), name, telephone, url, price.
- table Cuisine : type, id_resto (foreign key)
- table Rate : rating, reviews, id_resto (foreign key)

The information are set in a list in the program tripadvisor.py
by scrapping the tripadvisor website.
"""

import pymysql

connection = pymysql.connect(user='root', password='Macrogol16/')
cur = connection.cursor()

cur.execute("SHOW DATABASES")
for x in cur:
    if x[0] == 'db_tripadvisor':
        cur.execute("DROP DATABASE db_tripadvisor")

cur.execute("CREATE DATABASE db_tripadvisor")

cur.execute("USE db_tripadvisor")

cur.execute('''CREATE TABLE Restaurant
                               (id INT PRIMARY KEY,
                                name varchar(255),
                                address varchar(255),
                                telephone varchar(255),
                                url varchar(255),
                                price varchar(255),
                                latitude FLOAT,
                                longitude FLOAT
                                )''')
cur.execute('''CREATE TABLE Cuisine
                               (type varchar (255) ,
                                id_resto INT PRIMARY KEY,
                                FOREIGN KEY (id_resto) REFERENCES Restaurant(id)
                                )''')
cur.execute('''CREATE TABLE Rate
                               (rating FLOAT,
                                reviews INT,
                                id_resto INT PRIMARY KEY,
                                FOREIGN KEY (id_resto) REFERENCES Restaurant(id)
                                )''')
connection.commit()
cur.close()





